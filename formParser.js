$(document).ready(function(){
	$(".ajaxform").submit(function (evtObj){

		evtObj.preventDefault();
		
		var thisForm = this;
		
		var fields = [];
		
		var input = $(thisForm).find('input[type=text],input[type=date],input[type=email],input[type=checkbox], input[type=radio], input[type=hidden], input[type=file],  input[type=time],input[type=number], textarea');
		
		input.each(function(){
			var thisField = $(this);
			let type = $(thisField).attr('type')
			let val = '';
			if ((type == "radio" || type == "checkbox")) {
				if ($(thisField).is(':checked')) {
					val = $(thisField).parent().find('label').text();
				}
			} else {
				val = thisField.val() ? $(thisField).val() : $(thisField).attr('value');
			}
			fields.push({
				name: thisField.attr('name'),
				title: thisField.attr('title') || 'поле формы',

				value: val,
				require: (String($(thisField).attr('class'))).indexOf('require') !== -1,
				capcha: $(thisField).attr('capcha') || false,
				type: thisField.attr('type')
			});
			$(thisField.attr('errselector')).css({
				"opacity" : "0",
				"display" : "none"
			});
			thisField.css('border-color', '#d2d2d2');
		});
	
		var formData = new FormData();

		var inputFiles = $(thisForm).find('input[type=file]');
		if(inputFiles.length){
			for(var i = 0; i < inputFiles.length; i++){
				var files = inputFiles[i].files;
				for(var j = 0; j < files.length; j++){
					formData.append(files[j].name,  files[j]);
				}
			}
		}
		formData.append('fields',  JSON.stringify(fields));
		formData.append('title', $(thisForm).attr('title') || 'сообщение с формы vladimirsad.ru');
		formData.append('sendTo', $(thisForm).find('.ajaxSend').val());	
		formData.append('template', $('.ajaxMailTpl') ? String($('.ajaxMailTpl').html()) : false);
		
		var request;
		
        if (window.XMLHttpRequest)
		{
			request = new XMLHttpRequest();
        }
		else
		{
			request = new ActiveXObject("Microsoft.XMLHTTP");
        }
		
        request.onreadystatechange=function()
		{
			if (request.readyState==4 && request.status==200)
			{
				if(request.responseText == 'OK')
				{
					$(thisForm).animate({opacity: 0}, 500, function(){ $(thisForm).hide(500); });
					if($(thisForm).attr('okSelector'))
					{
						$($(thisForm).attr('okSelector')).show(500);
						$($(thisForm).attr('okSelector')).animate({opacity: 1}, 500, 'linear');
						$('.ok-mess').fadeIn();
					}
					else 
					{	
					
						$(thisForm).after('<p class="success">Сообщение отправлено успешно!</p>');
					}
					$(thisForm).find('input[type=text], textarea').val('');
				}
				else
				{
					console.log(request.responseText);
					var result = JSON.parse(request.responseText);
					for(var key in result){
						if(!result[key]){
							var errSelector = $(thisForm).find('[name="'+key+'"]').attr('errselector');
							if(errSelector)
							{
								$(thisForm).find(errSelector).show(500);
								$(thisForm).find(errSelector).animate({opacity: 1}, 500, 'linear');
							}
							else
							{
								$(thisForm).find('[name='+key+']').after('<p class="ajaxError">Данное поле пустое!</p>');
							}
							$(thisForm).find('[name='+key+']').css('border-color', '#ff5c5c');
						}
					}
				}
			}
		}
		let _formBM = $(thisForm).attr('class');		
		
		request.open("POST", "formParser.php",true);
		
		request.send(formData);

        return false;
	});
});