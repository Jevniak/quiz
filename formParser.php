<?php

if(!empty($_POST))
{
	$fields_array = $_POST;
	$fields = json_decode($fields_array['fields']);
	$title = $fields_array['title'];
	$sendTo = $fields_array['sendTo'];
	$template = $fields_array['template'];
	$capcha = json_decode($fields_array['capcha']);
	
	$boundary = "--".md5(uniqid(time()));
	
	$fromName = 'Менеджер';
	$to = "jevniak12@gmail.com";	
	$fromEmail = "jevniak12@gmail.com";
	// $to = "kochetkov@vladmedia.ru";
	// $fromEmail = "kochetkov@vladmedia.ru";	
	// $to = "aleksey@vladmedia.ru";
	// $fromEmail = "aleksey@vladmedia.ru";
	
	$headers = '';
	$headers .= "From: " . $fromName . " <" . $fromEmail . ">\n";
	$headers .= "To: " . $to . "\n";
	$headers .= "Subject: " . $title . "\n";
	$headers .= "X-Mailer: PHPMail Tool\n";
	//$headers .= "Reply-To: " . $fromEmail . "\n";
	$headers  = "MIME-Version: 1.0\n";
	$headers .= "Content-type: multipart/mixed; boundary=\"" . $boundary. "\"\n\n";
	
	$message = '';
	$body = '';
	$errors = array();
	
	forEach($fields as $field)
	{
		if(!$field->value)
		{
			if($field->require == "true")
			{
				$errors[$field->name] = false;
			}
			else
			{
				if($template)
				{
					$template = str_replace('['.$field->name.']', 'данные не указаны...', $template);
				}
			}
		} 
		else if($field->capcha)
		{
			if($field->value != $field->capcha)
			{
				$errors[$field->name] = false;
			}
		} 
		else 
		{
			if($field->type != "file"){
				$message .= $field->title.': '.htmlspecialchars(trim($field->value)).'<br>';
				if($template)
				{
					$template = str_replace('['.$field->name.']', $field->value , $template);
				}
			}
		}
		
	}
	
	if(!empty($_FILES)){
		forEach($_FILES as $files)
		{
			$fileName = $files['name'];
			$tmpFileName =  $files['tmp_name'];
			
			if (!empty($tmpFileName))
			{
				$path = $fileName;
				if (copy($tmpFileName, $fileName)) {
					$fileToRead = fopen($fileName, "r");
					$file = fread($fileToRead, filesize($fileName));
					fclose($fileToRead);
					unlink($fileName);
				}
				
				$body .= "--" . $boundary . "\n";
				$body .= "Content-Type: application/octet-stream; name==?utf-8?B?".base64_encode($fileName)."?=\n"; 
				$body .= "Content-Transfer-Encoding: base64\n";
				$body .= "Content-Disposition: attachment; filename==?utf-8?B?".base64_encode($fileName)."?=\n\n";
				$body .= chunk_split(base64_encode($file))."\n";
				
			}
		}
	}
	
	$body .= "--" . $boundary . "\n";
	$body .= "Content-type: text/html; charset=\"utf-8\"\n";
	$body .= "Content-Transfer-Encoding: quoted-printablenn\n\n";
	$body .= $message."\n";
	$body .= "--" . $boundary . "--\n";
	
	//print_r($body);
	
	if(!$errors)
	{
		if($template){
			$template = preg_replace('/\[(.+)\]/', 'параметра нет', $template);
		}
		$mail = mail($to, $title, $body, $headers);
		if($sendTo)
			if($template) mail($sendTo, $title, $template, $headers); 
			else mail($sendTo, $title, $body, $headers);
		
		if($mail)
		{
			echo 'OK';
		}

	}
	else
	{
		echo json_encode($errors);
	}

}
?>