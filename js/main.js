$(function () {
    var tabs = $('.tabs__item');
    $("#phone").mask("8(999) 999-9999");
    $('label').click(function () {
        let type = $(this).parent().find('input').attr('type');
        if (type == "radio") {
            $('label').removeClass('active');
            $(this).parent().find('input').click();
            $(this).addClass('active');
            tabID++;
            changeTab(tabID);
        } else if (type == "checkbox") {
            if ($(this).parent().find('input').is(':checked')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        }
    })

    var tabID = 0;
    $('.next__item').click(function (e) {
        e.preventDefault();
        tabID++;
        changeTab(tabID);
    })

    function changeTab(id) {
        tabs.fadeOut('slow',function () {

        });
        setTimeout(function () {
            $(tabs[id]).fadeIn('slow');

        }, 1000)

    }
})